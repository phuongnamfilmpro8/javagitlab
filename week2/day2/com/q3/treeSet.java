import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeSet;

public class treeSet {
    Scanner sc=new Scanner(System.in);
    public void addElemnt(TreeSet<String> arr) {
		System.out.println("Enter the number of element you want to add: ");
		int t = Integer.parseInt(sc.nextLine());
		for (int i = 0; i < t; i++) {
			int id = arr.size();
			System.out.println("Element "+(id+1)+": ");
			System.out.print("Enter the element value: ");
			String val = sc.next();
            if(!arr.add(val))
            {   i-=1;
                System.out.println("Element already exit");
            }
			
		}
	}
    public void viewAll(TreeSet<String> ts) {
        int count=0;
        for (String integer : ts) {
            count++;
            System.out.println("Element num "+count +": "+integer);
            
        }
    }
    public void viewFirst(TreeSet<String> ts) {
        System.out.println("First : "+ts.first());


    }
    public void viewLast(TreeSet<String> ts) {
        System.out.println("Last : "+ts.last());

    }
    public void RemoveLast(TreeSet<String> ts) {
        ts.pollLast();
    }
    public void getSize(TreeSet<String> ts) {
        System.out.println("Size : "+ts.size());
    }
    public void viewByIteratorInterface(TreeSet<String> ts) {
        Iterator<String> value = ts.iterator();
  
        System.out.println("The string values are: ");
        while (value.hasNext()) {
            System.out.println(value.next());
        }
    }

}
