import java.util.Deque;
import java.util.Scanner;

public class element {
    Scanner sc=new Scanner(System.in);

    public void addElemnt(Deque<Integer> arr) {
		System.out.println("Enter the number of element you want to add: ");
		int t = Integer.parseInt(sc.nextLine());
		for (int i = 0; i < t; i++) {
			int id = arr.size();
			System.out.println("Element "+(id+1)+": ");
			System.out.print("Enter the element value: ");
			int val = sc.nextInt();
		

			arr.add(val);
		}
	}

    public void addToHead(Deque<Integer> deque) {
        System.out.println("Add to Head \n Enter element value :");
        int k=sc.nextInt();
        deque.addFirst(k);
    }
    public void addToTail(Deque<Integer> deque) {
        System.out.println("Add to Tail \n Enter element value :");
        int k=sc.nextInt();
        deque.addLast(k);
    }
    public void viewAll(Deque<Integer> s)
    {
        int count=0;
        for (Integer integer : s) {
            count++;
            System.out.println("Element num "+count +": "+integer);
            
        }
    }

    public void viewFirst(Deque<Integer> deque) {
        
        System.out.println("the first element "+deque.peek());
    }

    public void clear(Deque<Integer> deque) {
        deque.clear();
        System.out.println("Done ! ");
    }
    
}
